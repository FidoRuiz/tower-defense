﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelSelectButton : MonoBehaviour {
    [SerializeField] private GameObject levelbutton;
    [SerializeField] private GameObject quitbutton;
    [SerializeField] private GameObject scrollbutton;
    [SerializeField] private GameObject goback;
    [SerializeField] private GameObject quit;
    [SerializeField] private GameObject levelselect;

    public GameObject cameralevel;

    void Start ()

    {

       scrollbutton.SetActive(false);
    }


    public void buttonclick()

    {
        cameralevel.GetComponent<Animation>().Play();
        levelbutton.SetActive(false);
        quitbutton.SetActive(false);
        Invoke("scrollbuttonmethod", 1.0f);

        
    }
    public void gobackbutton()
    {
        
        scrollbutton.SetActive(false);
        cameralevel.GetComponent<Animation>().Play("Camera_Animation2");
        Invoke("levelbuttons", 1.0f);

    }
    public void level_load()

    {
        SceneManager.LoadScene("GameSceneFernandoRuiz");

    }

    public void quitgame()
    {
        Application.Quit();
    }

    public void scrollbuttonmethod()
    {
        scrollbutton.SetActive(true);
    }

    public void levelbuttons()
    {
        levelbutton.SetActive(true);
        quitbutton.SetActive(true);
    }

}
